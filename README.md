# How to submit your project output for stage 2
1) Open <code>pom.xml</code> and change the artifect ID from <code>2016000000</code> to your student ID.
1) Complete <code>edu.hanyang.submit.TinySEExternalSort.java</code> file.
1) Run <code>mvn package</code>.
1) If you pass every unit test successfully, upload <code>&lt;your student ID&gt;-0.0.1-SNAPSHOT.jar</code> file on the web board.
