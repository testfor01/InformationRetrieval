package lagacy.util.bptree0;

import java.nio.file.StandardOpenOption;
import java.util.Queue;

import org.apache.commons.lang3.tuple.Triple;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.LinkedBlockingQueue;

public class StaticExternalBPTree implements BPTree, Closeable {
	
	private FileChannel in;
	private FileChannel out;
	private StaticExternalBPTreeNode node;
	private int nodeSize;
	private int bufferSize;
	private ByteBuffer buffer;
	private byte[] swap;
	private int leafNodeCount;
	private int leafDepth;
	
	public StaticExternalBPTree(int nodeSize, int bufferSize, File file) throws FileNotFoundException, IOException{
		boolean isnew = file.createNewFile();
		node = new StaticExternalBPTreeNode(nodeSize,0,0,true);
		this.bufferSize = bufferSize;
		this.nodeSize = nodeSize;
		this.swap = new byte[getNodeBytes()];
		if(nodeSize <2){
			throw new RuntimeException("'nodeSize' must be 2 at least");
		}else if(bufferSize < nodeSize * 8 + 4){
			throw new RuntimeException("'bufferSize' must be "+ nodeSize * 8 + 4 + " byte at least.");
		}else{
			this.buffer = ByteBuffer.allocate(bufferSize);
			in = FileChannel.open(file.toPath(), StandardOpenOption.READ);
			out= FileChannel.open(file.toPath(), StandardOpenOption.WRITE);
			if(isnew){
				node.write(out, buffer);
				leafDepth = 0;
				leafNodeCount = 0;
			}else{
				int nodeCount = (int)(file.length()/getNodeBytes());
				leafDepth = logn(BigInteger.valueOf(nodeCount).multiply(BigInteger.valueOf(nodeSize)).add(BigInteger.ONE).intValue(), nodeSize+1)-1;
				leafNodeCount = nodeCount - BigInteger.valueOf(nodeSize+1).pow(leafDepth).subtract(BigInteger.ONE).divide(BigInteger.valueOf(nodeSize)).intValue();
			}
		}		
	}
	
	private int getLeafMaxOrder() throws IOException{
		int maxOrder = 0;
		int depth = 0;
		while(depth < leafDepth){
			getBPTreeNode(depth++,maxOrder);
			maxOrder = StaticExternalBPTreeNode.getRightOrder(depth, maxOrder, node.size-1);
		}
		return maxOrder;
	}
	
	private void resetInput() throws IOException{
		resetOutput(0,0);
	}
	
	private void resetInput(int depth, int order) throws IOException{
		in.position(getNodeBytes()*StaticExternalBPTreeNode.getIndex(nodeSize, depth, order));
	}
	
	private void resetOutput(int depth, int order) throws IOException{
		out.position(getNodeBytes()*StaticExternalBPTreeNode.getIndex(nodeSize, depth, order));
	}
	
	private void swap(){
		byte[] b = buffer.array();
		for(int i=0; i<swap.length; i++){
			b[i] = (byte)(swap[i] ^ b[i]);
			swap[i] = (byte)(swap[i] ^ b[i]);
			b[i] = (byte)(swap[i] ^ b[i]);
		}
	}
	
	public int getNodeBytes(){
		return nodeSize * 8 + 4;
	}
	
	//쉬프트 버퍼를 늘리는걸 고려하자..
	// 고려사항 한가지 더 : swap버퍼사이즈가 커지면 커질수록 fan-out감소, 쉬프팅속도 증대, 어떤것이 큰 것이 좋을 지 모름.
	public void shift(int depth, int order, int nodeCount) throws IOException{
		resetInput(depth,order);
		resetOutput(depth,order);
		buffer.limit(swap.length);
		while(nodeCount > 0){
			buffer.position(0);
			in.read(buffer);
			swap();
			buffer.position(0);
			out.write(buffer);
			nodeCount--;
		}
	}
	
	public void copy(int depth, int lastOrder, boolean isParentSplit, ExternalBNode splitedNode) throws IOException{
		int remainCount = lastOrder - (splitedNode.getOrder()+1);
		int remainParam = (remainCount==0)?1:0;
		if(!isParentSplit){
			shift(depth, splitedNode.getOrder()+1, lastOrder -splitedNode.getOrder());
			resetOutput(depth,splitedNode.getOrder());
			node.write(out, buffer);
			splitedNode.write(out, buffer);
			if(depth != leafDepth){
				hardSaparate(depth+1,
						StaticExternalBPTreeNode.getLeftOrder(nodeSize, splitedNode.getOrder()+1, 0),
						StaticExternalBPTreeNode.getLeftOrder(nodeSize, splitedNode.getOrder()+2, 0),
						(nodeSize+1) * remainCount
				);
				hardSaparate(depth+1,
						StaticExternalBPTreeNode.getRightOrder(nodeSize, splitedNode.getOrder(), node.size-1),
						StaticExternalBPTreeNode.getLeftOrder(nodeSize, splitedNode.getOrder()+1, 0),
						nodeSize+remainParam - node.size + 1
						//lastOrder + 1 - (nodeSize+1)*splitedNode.getOrder()+nodeSize
				);
			}
		}else{
//			remainCount = (splitedNode.getOrder() <= nodeSize/2)?1:0;
//			remainCount = (remainCount+nodeSize+1)/2;
//			
//			
//			if(depth == 0){
//				remainCount = -1;
//			}
			resetInput(depth,0);
			resetOutput(depth+1,0); 
//			remainCount = moveWithSkip(splitedNode.getOrder(), remainCount, nodeSize-remainCount+1);
//			node.write(out, buffer);
//			if(remainCount == 1){
//				skip(nodeSize-remainCount+1);
//				remainCount = -1;
//			} 
//			splitedNode.write(out, buffer);
//			if(remainCount == 2){
//				skip(nodeSize-remainCount+1);
//				remainCount = -1;
//			}
//			remainCount = moveWithSkip(BigInteger.valueOf(nodeSize+1).pow(depth).intValue() - (splitedNode.getOrder() + 1), remainCount, nodeSize-remainCount+1);
			int k = splitedNode.getOrder() % (nodeSize+1);
			int d = 0;
			int pLeft = getPSize(k,true);
			
			move(splitedNode.getOrder());
			node.write(out, buffer);
			skip(in,1);
			// 이 이후 뒤로 밀려남 해결필수
			// splitedNode.write가 왜 이상하게 쓰여지는지 검증.
			if(k==pLeft){
				skip(out,1);
				splitedNode.write(out, buffer);
				skip(out,nodeSize-1);
				move(BigInteger.valueOf(nodeSize+1).pow(depth).intValue() - (splitedNode.getOrder()) - nodeSize);
			}else{
				splitedNode.write(out, buffer);
				skip(out,nodeSize);
				move(BigInteger.valueOf(nodeSize+1).pow(depth).intValue() - (splitedNode.getOrder()) - nodeSize);
			}
			//skip(out,2*(nodeSize+1) - (k+2));
			if(depth != 0){
				//int i = splitedNode.getOrder() - k;
				d = (splitedNode.getOrder()/(nodeSize+1)  == BigInteger.valueOf(nodeSize+1).pow(depth-1).intValue()-1)?1:0;
				//int pRight= nodeSize + 1 - pLeft;
				hardSaparate(depth+1,
						splitedNode.getOrder()-k+pLeft,
						nodeSize+1+splitedNode.getOrder()-k,
						nodeSize+1+d-pLeft
				);
//				if(k==pLeft){
//					hardSaparate(depth+1,
//							splitedNode.getOrder()+1,
//							splitedNode.getOrder()+2,
//							BigInteger.valueOf(nodeSize+1).pow(depth-1).intValue()+1
//					);
//				}
			}
//			int moveOrder = StaticExternalBPTreeNode.getLeftOrder(nodeSize, StaticExternalBPTreeNode.getParentOrder(nodeSize, splitedNode.getOrder())+1, 0);
////			if(moveOrder >= lastOrder)
////				moveOrder--;
		}

		
		
		
			
		if(node.isLeaf && isParentSplit){
			leafDepth++;
		}
		System.out.println(depth + "에서 " + (depth+1) );
		System.out.println("[[[[중간]]]]");
		System.out.println("[[[[중간]]]]");
		System.out.println("[[[[중간]]]]");
		show(true);
		System.out.println("[[[[[]]]]]");
		System.out.println("[[[[[]]]]]");
		System.out.println();
		System.out.println();
		
		
	}
	
	public void copy(int depth, int targetDepth) throws IOException{
		resetInput(depth,0);
		resetOutput(targetDepth,0);
		move(BigInteger.valueOf(nodeSize+1).pow(depth).intValue());
	}
	
	private int getPSize(int childIndex, boolean isLeft){
		int result = 0;
		if(nodeSize % 2 == 0)
			result = (childIndex < nodeSize/2)?(nodeSize/2)+1:nodeSize/2;
		else
			result = (nodeSize+1)/2;
		if(isLeft)
			return result;
		else
			return nodeSize+1 - result;
	}
	
	private void skip(FileChannel c, int index) throws IOException{
		c.position(c.position() + getNodeBytes()*index);
	}
	
	private void shallowSaperate(int depth, int order, int targetOrder, int unitCounts) throws IOException{
		
		if(targetOrder-order < unitCounts){
			shallowSaperate(depth, targetOrder, 2 * targetOrder - order, unitCounts - (targetOrder-order));
			unitCounts = targetOrder-order;
		}
		resetInput(depth, order);
		resetOutput(depth,targetOrder);
		move(unitCounts);
		
	}
	
	private void hardSaparate(int depth, int order, int targetOrder, int unitCounts) throws IOException{
		System.out.println("depth : " + depth + " and order : " + order + " and target: " + targetOrder + " and count : " + unitCounts + "// leafDepth = " + leafDepth);
		do{
			if(order == targetOrder || unitCounts == 0)
				break;
			shallowSaperate(depth,order,targetOrder,unitCounts);
			if(depth == leafDepth){
				break;
			}else{
				depth++;
				order = StaticExternalBPTreeNode.getLeftOrder(nodeSize, order, 0);
				targetOrder = StaticExternalBPTreeNode.getRightOrder(nodeSize, targetOrder, 0);
				unitCounts *= nodeSize+1;
			}
		}while(depth<leafDepth);
	}

	private void arrangeRootNode(ExternalBNode splitedNode) throws IOException{
		ExternalBNode root = new StaticExternalBPTreeNode(nodeSize,0,0,false);
		root.insert(splitedNode.getKey(0), splitedNode.getValue(0));
		resetOutput(0,0);
		root.write(out, buffer);
	}
	
	private void move(int unitCounts) throws IOException{
		int remain = unitCounts * getNodeBytes();
		buffer.limit(buffer.capacity());
		while(remain > 0){
			if(remain < bufferSize){
				buffer.limit(remain);
			}
			buffer.position(0);
			if(in.read(buffer) == 0)
				throw new EOFException();
			buffer.flip();
			remain -= out.write(buffer);
		}
	}
	
	public void getBPTreeNode(int depth, int order) throws IOException{
		node.depth = depth;
		node.order = order;
		node.isLeaf = depth==leafDepth;
		resetInput(depth,order);
		node.read(in,buffer);
	}
	
	public int readSizeFromNode(int depth, int order) throws IOException{
		resetInput(depth,order);
		buffer.limit(4);
		buffer.position(0);
		in.read(buffer);
		buffer.flip();
		return buffer.getInt();
	}
	
	private int goToLeaf(int key) throws IOException, NodeDataNotFound{
		resetInput();
		int depth = 0;
		int order = 0;
		while(true){
			getBPTreeNode(depth++,order);
			try{
				return node.get(key);
			}catch(NodeDataNotFound e){
				switch(e.flag){
					case 0:
						throw e;
					case 1:
						order = e.order;
						break;
					default:
						throw new RuntimeException("unsupported flag : " + e.flag);
				}
			}
		}
	}
	
	private int logn(int x, int n){
		int result = 0;
		while(x>1){
			x = StaticExternalBPTreeNode.ceil_div(x,n);
			result++;
		}
		return result;
	}

	@Override
	public int get(int key) throws IOException, NodeDataNotFound{
		return goToLeaf(key);
	}

	@Override
	public int insert(int key, int value) throws IOException {
		try{
			int v = goToLeaf(key);
			throw new RuntimeException("Violated unique key policy. [Duplicated key : (" + key + "," + v + ")]" );
		}catch(NodeDataNotFound e){
				int targetKey = key;
				int targetValue = value;
				int targetDepth = e.depth;
				int targetOrder = e.order;
				int lastOrder = getLeafMaxOrder()+1;
				int parentSize = 0;
				int newLastOrder = lastOrder;
				ExternalBNode splited;
				while(targetDepth >= 0){
					if(targetDepth > 0)
						parentSize = readSizeFromNode(targetDepth-1, targetOrder/(nodeSize+1));
					getBPTreeNode(targetDepth, targetOrder);
					splited  = (ExternalBNode)node.insert(targetKey, targetValue);
					if(splited != null){
						if(targetDepth == leafDepth)
							this.leafNodeCount++;
						
						// lastIndex = (노드사이즈+1)*부모노드갯수 - 부모의 마지막 노드 빈자리
						copy(
							targetDepth,lastOrder, 
								targetDepth == 0 
									|| 
								(
									parentSize==nodeSize 
										&& 
									((newLastOrder = StaticExternalBPTreeNode.ceil_div(lastOrder,nodeSize+1)) == BigInteger.valueOf(nodeSize+1).pow(targetDepth-1).intValue())
								)
							,splited);
						lastOrder = newLastOrder;
						targetKey = splited.getKey(0);
						targetValue = splited.getValue(0);
						targetDepth--;
						targetOrder = StaticExternalBPTreeNode.getParentOrder(nodeSize, targetOrder);
						
						if(targetDepth < 0){
							arrangeRootNode(splited);
							break;
						}
					}else{
						if(leafNodeCount==0)
							this.leafNodeCount++;
						resetOutput(node.depth,node.order);
						node.write(out, buffer);
						return key;
					}
			}
			return key;
		}
	}

	@Override
	public int delete(int key){
		throw new RuntimeException("Not Yet");
	}

	@Override
	public boolean isEmpty() {
		return leafNodeCount==0;
	}

	@Override
	public int nodeSize() {
		return nodeSize;
	}
	
	public void show() {
		show(false);
	}

	public void show(boolean isRaw) {
		System.out.println("========================================");
		System.out.println("Total Depth : " + (leafDepth + 1));
		System.out.println("Total LeftNode : " + leafNodeCount);
		Queue<Triple<Integer,Integer,Boolean>> queue = new LinkedBlockingQueue<Triple<Integer,Integer,Boolean>>();
		queue.add(Triple.of(0, 0, true));
		while(!queue.isEmpty())
			show(queue, isRaw);
		System.out.println("========================================");
	}
	
	public void show(Queue<Triple<Integer,Integer,Boolean>> queue, boolean isRaw) {
		Triple<Integer,Integer,Boolean> p = queue.remove();
		try {
			getBPTreeNode(p.getLeft(), p.getMiddle());
			System.out.println("(" + p.getLeft() + "," + p.getMiddle() + ") : " + node + " and leaf : " + node.isLeaf);
			int range = (isRaw)?nodeSize+1:node.size;
			if(!node.isLeaf){
				for(int i=0; i<range; i++)
					queue.add(Triple.of(p.getLeft()+1, StaticExternalBPTreeNode.getLeftOrder(nodeSize, p.getMiddle(), i),false));
				if(p.getRight() && !isRaw)
					queue.add(Triple.of(p.getLeft()+1, StaticExternalBPTreeNode.getRightOrder(nodeSize, p.getMiddle(), node.size-1),true));
			}
		} catch (Exception e) {
		}
	}
	
	public void close() throws IOException{
		in.close();
		out.close();
	}
	
}
