package lagacy.util.bptree0;

import java.io.IOException;

public interface BPTree{
	
	public int get(int key) throws IOException, NodeDataNotFound;
	
	public int insert(int key, int value) throws IOException;
	
	public int delete(int key);
	
	public boolean isEmpty();
	
	public int nodeSize();

}
