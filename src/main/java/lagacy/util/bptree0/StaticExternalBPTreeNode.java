package lagacy.util.bptree0;

import java.math.BigInteger;
import java.util.Arrays;

public class StaticExternalBPTreeNode extends ExternalBNode{
	
	public StaticExternalBPTreeNode(int nodeSize, int depth, int order, boolean isLeaf) {
		super(nodeSize, depth, order, isLeaf);
	}

	public StaticExternalBPTreeNode(int nodeSize, int depth, int order) {
		super(nodeSize, depth, order);
	}
	
	@Override
	public String toString() {
		String s = "[";
		for(int i=0; i<size; i++){
			s = s.concat("("+keys[i] + "," + values[i] + ")" + ","); 
		}
		
		s = s.concat("end]");
		return s;
	}

	public void insert_unit(int key, int value) {
		keys[size] = key;
		values[size] = value;
		for(int i=size-1; i>=0 && key < keys[i]; i--){
			swap(i,i+1);
		}
		size++;
	}
	
	private void move(ExternalBNode from, ExternalBNode to, int startIndex){
		for(int i=startIndex; i<from.size; i++){
			to.keys[i-startIndex] = from.keys[i];
			to.values[i-startIndex] = from.values[i];
		}
	}

	@Override
	public BNode insert(int key, int value) {
		if(size < keys.length){
			insert_unit(key,value);
			return null;
		}else{
			int standard = size/2;
			//스플릿(복제본) 생성.
			StaticExternalBPTreeNode splitedNode = new StaticExternalBPTreeNode(size, depth, order, isLeaf);
			if(key < keys[standard]){
				// 스플릿(원본)에 key,value를 삽입
				move(this,splitedNode,standard);
				splitedNode.size = this.size - standard;
				this.size = standard;
				this.insert_unit(key, value);
			}else{
				// 스플릿(복제본)에 key,value를 삽입
				if(size % 2 == 0)
					standard--;
				move(this,splitedNode,standard+1);
				splitedNode.size = this.size - (standard+1);
				this.size = standard + 1;
				splitedNode.insert_unit(key, value);
			}
			return splitedNode;
		}
	}

	@Override
	public BNode delete(BNode from, int key, int value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int get(int key) throws NodeDataNotFound {
		int i = Arrays.binarySearch(keys, 0, size, key);
		if(i>=0){
			return values[i];
		}else{
			if(isLeaf){
				throw new NodeDataNotFound(0, depth, order);
			}else{
				throw new NodeDataNotFound(1,depth+1, (keys.length+1)*order + -i-1);
			}
		}
	}
	
	public static int getIndex(int nodeSize, int depth, int order){
		return BigInteger.valueOf(nodeSize+1).pow(depth).subtract(BigInteger.valueOf(1)).divide(BigInteger.valueOf(nodeSize)).intValue() + order;
	}
		
	private void swap(int i, int j){
		keys[i] = keys[i] ^ keys[j];
		keys[j] = keys[i] ^ keys[j];
		keys[i] = keys[i] ^ keys[j];
		values[i] = values[i] ^ values[j];
		values[j] = values[i] ^ values[j];
		values[i] = values[i] ^ values[j];
	}
	
	public static int ceil_div(int x, int y) {
	    return (int)(x / y) + ((x % y > 0)?1:0);
	}
	
	public static int getLeftOrder(int nodeSize, int order, int index){
		return (nodeSize+1) * order + index;
	}
	
	public static int getRightOrder(int nodeSize, int order, int index){
		return getLeftOrder(nodeSize, order, index) + 1;
	}
	
	public static int getParentOrder(int nodeSize, int order){
		return order / (nodeSize+1);
	}
	
}
