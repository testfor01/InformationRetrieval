package lagacy.util.bptree0;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;


public abstract class ExternalBNode implements BNode{
	
	protected int size;
	
	protected int depth;
	
	protected int order;
	
	protected int[] keys;
	                                                                                                                                                                                                                                                                                                                                                                                                         
	protected int[] values;
	
	protected boolean isLeaf;
	
	protected ExternalBNode(int nodeSize, int depth, int order){
		this(nodeSize, depth, order, true);
	}
	
	protected ExternalBNode(int nodeSize, int depth, int order, boolean isLeaf){
		this.size = 0;
		this.keys = new int[nodeSize];
		this.values = new int[nodeSize];
		this.order = order;
		this.isLeaf = isLeaf;
	}
	
	public int write(FileChannel out, ByteBuffer b) throws IOException{
		int bufferSize = b.capacity();
		b.limit(bufferSize - bufferSize % 4);
		b.position(0);
		writeInt(out,b,size);
		for(int i=0; i<keys.length; i++){
			writeInt(out,b,keys[i]);
			writeInt(out,b,values[i]);
		}
		flush(out,b);
		return keys.length;
	}
	
	public int read(FileChannel in, ByteBuffer b) throws IOException{
		int bufferSize = b.capacity();
		b.limit(4);
		b.position(0);
		in.read(b);
		b.flip();
		size = b.getInt();
		int limit = 8*size;
		int index = 0;
		b.limit((bufferSize > limit)?limit:bufferSize - bufferSize % 4);
		b.position(b.limit());
		for(int i=0; i<size; i++){
			keys[i] = readInt(in,b,limit,index);
			index += 4;
			values[i] = readInt(in,b,limit,index);
			index += 4;
		}
		in.position(in.position() + (keys.length-size)*8);
		return size;
	}
	
	private int readInt(FileChannel in, ByteBuffer buffer, int limit, int index) throws IOException{
		try{
			return buffer.getInt();
		}catch(BufferUnderflowException e){
			if(buffer.position() > limit-index){
				buffer.limit(limit-index);
				buffer.position(0);
			}else{
				buffer.flip();
			}
			in.read(buffer);
			buffer.position(0);
			return buffer.getInt();
		}
	}
	
	private void writeInt(FileChannel out, ByteBuffer buffer, int value) throws IOException{
		try{
			buffer.putInt(value);
		}catch(BufferOverflowException e){
			flush(out, buffer);
			buffer.putInt(value);
		}
	}
	
	private void flush(FileChannel out, ByteBuffer buffer) throws IOException{
		buffer.flip();
		out.write(buffer);
		buffer.position(0);
	}
	
	@Override
	public int getDepth() {
		return depth;
	}

	@Override
	public int getOrder() {
		return order;
	}
	
	@Override
	public int getSize() {
		return size;
	}
	
	@Override
	public int getMaxSize() {
		return keys.length;
	}
	
	@Override
	public boolean isLeaf() {
		return isLeaf;
	}
	
	@Override
	public int getKey(int index) {
		return keys[index];
	}
	
	@Override
	public int getValue(int index) {
		return values[index];
	}
	
}
