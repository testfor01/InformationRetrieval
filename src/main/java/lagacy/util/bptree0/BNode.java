package lagacy.util.bptree0;

interface BNode {
	
	public BNode insert(int key, int value);
	
	public BNode delete(BNode from, int key, int value);
	
	public int get(int key) throws NodeDataNotFound;
	
	public int getDepth();
	
	public int getOrder();
	
	public int getSize();
	
	public int getMaxSize();
	
	public boolean isLeaf();
	
	default boolean isFull(){
		return getSize() == getMaxSize();
	}
	
	public int getKey(int index) throws NullPointerException;
	
	public int getValue(int index) throws NullPointerException;

}
