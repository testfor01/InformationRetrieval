package edu.hanyang.submit;

import java.io.File;
import java.io.IOException;

import edu.hanyang.indexer.BPlusTree;
import edu.hanyang.util.NodeDataNotFound;
import edu.hanyang.util.StaticExternalBPTree;

public class TinySEBPlusTree implements BPlusTree{
	
	public StaticExternalBPTree bptree;

	@Override
	public void close() {
		try {
			bptree.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void insert(int key, int value) {
		try {
			bptree.familyInsert(key, value);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void open(String metafile, String file, int blocksize, int nblock) {
		long memory = blocksize*nblock;
		long penalty = (int) (((memory/8 < 4)?4:memory/8) + ((memory>32 && memory<64)?memory/32:0));
		int nodeSize = (int)(((memory-penalty)*45000)/1048576);
		try {
			bptree = new StaticExternalBPTree(nodeSize, new File(file), new File(metafile));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public int search(int key) {
		try {
			return bptree.get(key);
		} catch (IOException e) {
			return -2;
		} catch (NodeDataNotFound e) {
			return -1;
		}
	}

}
