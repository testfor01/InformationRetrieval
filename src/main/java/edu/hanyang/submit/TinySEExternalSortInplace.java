package edu.hanyang.submit;
 
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.MutableTriple;

import edu.hanyang.utils.DiskIO;
import edu.hanyang.indexer.ExternalSort;
 
public class TinySEExternalSortInplace implements ExternalSort {
	
	public static String tmpfile = "/tmp.data";
	
	public static class MyTriplet extends MutableTriple<Integer, Integer, Integer>{

		private static final long serialVersionUID = 2801436097965862459L;

		public MyTriplet(int left, int middle, int right) {
			super(left,middle,right);
		}
		
		
	}
	
	private MyTriplet[] buff; 
	private MyTriplet[] memory;
	
	private DataInputStream bsin;
	private DataOutputStream bsout;
	
	
	
	public TinySEExternalSortInplace(){
		buff = new MyTriplet[2];
		buff[0] = new MyTriplet(0,0,0);
		buff[1] = new MyTriplet(0,0,0);
	}
	
	public void sort(String infile, String outfile, String tmpdir, int blocksize, int nblocks) throws IOException {
		// complete the method
		System.out.println("SORT START");
		mkdir(tmpdir);
		int memory_num = get_buffnum(blocksize, nblocks);
		
		memory = new MyTriplet[memory_num];
		for(int i=0; i<memory_num; i++)
			memory[i] = new MyTriplet(0,0,0);
		bsin = DiskIO.open_input_run(infile, blocksize);
		int total = bsin.available()/12;
		int interval = memory_num*12;
		int max_way = (blocksize*nblocks - 24)/4;
		int runs = ceil_div(total,memory_num);
		
		int pos = (ceil_div(Math.log(runs),Math.log(max_way)) - 1) % 2;
		if(total == Integer.MAX_VALUE){
			throw new EOFException();
		}
		if(pos % 2 ==0){
			bsout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(tmpdir + tmpfile),blocksize));
		}else{
			bsout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outfile),blocksize));
		}
		
		init_sort(bsin,bsout,memory,memory_num);
		bsin.close();
		bsout.close();
		
		System.out.println("TOTAL INITIAL RUNS : " + runs);
		System.out.println("AVAILABLE MAX_WAYS : " + max_way + " BY MEMORY SPACE");
		if(max_way > runs){
			max_way = runs;
		}
		System.out.println("MERGE SORT OPTIONS : " + max_way + "-way merge sort at least");
		int remains = 0;
		int cnt;
		String path;
		for(; interval/12<total; pos++){
			if(pos % 2 == 0){
				path = tmpdir + tmpfile;
				bsout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outfile),blocksize));
			}else{
				path = outfile;
				bsout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(tmpdir + tmpfile),blocksize));
			}
			/*
			 * finish this */
			remains = runs % max_way;
			runs = ceil_div(runs,max_way);
			merge(path,blocksize,bsout,max_way,buff,0,interval);
			for(cnt=1; cnt<runs; cnt++){
				if(cnt == runs-1){
					merge(path,blocksize,bsout,remains,buff,cnt*interval*max_way,interval);
				}else{
					merge(path,blocksize,bsout,max_way,buff,cnt*interval*max_way,interval);
				}
					
			}
			interval = interval*max_way;
			bsout.close();
			memory = null;
			System.gc();
		}
	}
	
	private int init_sort(DataInputStream in, DataOutputStream out, MyTriplet[] memory, int memory_num) throws IOException{
		int count, runs = 0;
		try{
			while((count = read(in, memory_num, memory)) > 0){
				
				Arrays.sort(memory,0,count);
				append(out, memory, count);
				runs++;
			}
		}
		catch(EOFException e){
			
		}
		out.flush();
		return runs;
	}
	
	private int get_buffnum(int blocksize, int nblocks){
		if(nblocks < 3 && blocksize < 1){
			throw new RuntimeException("Not Enough Memory");
		}
		else if(nblocks * blocksize < 30){
			throw new RuntimeException("Not Enough Memory");
		}
		return (nblocks * blocksize) / 12;
	}
	
	private static void merge(String inpath, int buffersize, DataOutputStream out, int ways, MyTriplet[] buff, int start, int interval) throws IOException{
		int[] indexs = new int[ways];
		Arrays.fill(indexs, 0);
		boolean remain = false;
		
		do{
			DataInputStream in = DiskIO.open_input_run(inpath, buffersize);
			in.skipBytes(start);
			remain = false;
			int minIndex = 0;
			boolean first = true;
			for(int i = 0; i<indexs.length; i++){
				if(indexs[i]<interval){
					try{
						in.skipBytes(indexs[i]);
						if(first){
							readUnit(buff[1],in);
							remain = true;
							minIndex = i;
							first = false;
						}
						else{
							readUnit(buff[0],in);
							remain = true;
							if(buff[0].compareTo(buff[1])<0){
								buff[1].setLeft(buff[0].getLeft());
								buff[1].setMiddle(buff[0].getMiddle());
								buff[1].setRight(buff[0].getRight());
								minIndex = i;
							}
						}
						in.skipBytes(interval-12-indexs[i]);
					}catch(EOFException e){
					}
				}else{
					in.skipBytes(interval);
				}
			}
			if(remain){
				indexs[minIndex] += 12;
				append(out,buff[1]);
			}
			in.close();
		}while(remain);
		indexs = null;
		System.gc();
	}
	
	public static void readUnit(MutableTriple<Integer, Integer, Integer> triple, DataInputStream in) throws EOFException,IOException{
		triple.setLeft(in.readInt());
		triple.setMiddle(in.readInt());
		triple.setRight(in.readInt());
	}
	
	public static int read(DataInputStream in, int limit, MyTriplet[] arr) throws IOException {
		int i = 0;
		try{
			for(; i<limit; i++){
				readUnit(arr[i],in);
			}
		}catch(EOFException e){
		}
		return i;
	}
	
	public static void append(DataOutputStream out, MyTriplet[] arr, int records) throws IOException{
		for (int i=0; i<records; i++) {
			append(out,arr[i]);
		}
	}
	
	public static void append(DataOutputStream out, MyTriplet elem) throws IOException{
		out.writeInt(elem.getLeft());
		out.writeInt(elem.getMiddle());
		out.writeInt(elem.getRight());
	}
	
	public static void mkdir(String dir){
		File f = new File(dir);
		if(!f.exists()){
			f.mkdirs();
		}
	}
	
	public static int ceil_div(int x, int y) {
	    return (int)(x / y) + ((x % y > 0)?1:0);
	}
	
	public static int ceil_div(double x, double y) {
	    return (int)(x / y) + ((x % y > 0)?1:0);
	}
	

	
}
