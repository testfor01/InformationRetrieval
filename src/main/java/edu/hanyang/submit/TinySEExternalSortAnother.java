package edu.hanyang.submit;
 
import java.io.BufferedOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.tuple.MutableTriple;

import edu.hanyang.utils.DiskIO;
import edu.hanyang.indexer.ExternalSort;
 
public class TinySEExternalSortAnother implements ExternalSort {
	
	public static String tmpfile = "/tmp.data";
	

	
	private MutableTriple<Integer, Integer, Integer> inbuff; 
	private MutableTriple<Integer, Integer, Integer> outbuff; 
	private List<MutableTriple<Integer, Integer, Integer>> memory;
	
	private DataInputStream bsin;
	private DataOutputStream bsout;
	
	private ExecutorService merger;
	
	
	
	public TinySEExternalSortAnother(){
		inbuff = new MutableTriple<Integer, Integer, Integer>(0,0,0);
		outbuff = new MutableTriple<Integer, Integer, Integer>(0,0,0);
	}
	
	public void sort(String infile, String outfile, String tmpdir, int blocksize, int nblocks) throws IOException {
		// complete the method
		System.out.println("SORT START");
		mkdir(tmpdir);
		
		int i;
		int memory_num = get_buffnum(blocksize, nblocks);
		String inpath,outpath;
		
		memory = new ArrayList<MutableTriple<Integer, Integer, Integer>>(memory_num);
		for(i=0; i<memory_num; i++){
			memory.add(new MutableTriple<Integer, Integer, Integer>(0,0,0));
		}
		bsin = DiskIO.open_input_run(infile, blocksize);
		
		int total = bsin.available()/12;
		int interval = memory_num*12;
		int max_way = (blocksize*nblocks - 24)/4;
		int runs = ceil_div(total,memory_num);
		int pos = (ceil_div(Math.log(runs),Math.log(max_way)) - 1) % 2;
		
		if(total == Integer.MAX_VALUE){
			throw new EOFException();
		}
		if(pos % 2 ==0){
			outpath = tmpdir + tmpfile;
		}else{
			outpath = outfile;
		}
		bsout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outpath),blocksize));
		
		init_sort(bsin,bsout,memory,memory_num);
		
		
		
		bsin.close();
		bsout.close();
		memory = null;
		System.gc();
		
		System.out.println("TOTAL INITIAL RUNS : " + runs);
		System.out.println("AVAILABLE MEMORIES PER TRANSACTION : " + memory_num);
		System.out.println("AVAILABLE MAX_WAYS : " + max_way + " BY MEMORY SPACE");
		if(max_way > runs){
			max_way = runs;
		}
		System.out.println("MERGE SORT OPTIONS : " + max_way + "-way merge sort at least");
		int remains = 0;
		int cnt;
		
		for(; interval/12<total; pos++){
//			System.out.println("SET POS : " + pos);
			synchronized(this){
				merger = Executors.newFixedThreadPool(1);
				if(pos % 2 == 0){
					inpath = tmpdir + tmpfile;
					outpath = outfile;
				}else{
					inpath = outfile;
					outpath = tmpdir + tmpfile;
				}
				/*
				 * finish this */
				remains = (runs % max_way == 0)?max_way:runs%max_way;
				runs = ceil_div(runs,max_way);
				asyncMerge(merger,inpath,outpath,blocksize,max_way,0,interval,0);
				for(cnt=1; cnt<runs; cnt++){
					if(cnt == runs-1){
						asyncMerge(merger,inpath,outpath,blocksize,remains,cnt*interval*max_way,interval,cnt);
					}else{
						asyncMerge(merger,inpath,outpath,blocksize,max_way,cnt*interval*max_way,interval,cnt);
					}
				}
				merger.shutdown();
				try {
					merger.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
					interval = interval*max_way;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void init_sort(DataInputStream in, DataOutputStream out, List<MutableTriple<Integer, Integer, Integer>> memory, int memory_num) throws IOException{
		int count = 0;
		try{
			while((count = read(in, memory_num, memory)) > 0){
				Collections.sort(memory.subList(0, count));
				append(out, memory, count);
			}
		}
		catch(EOFException e){
			
		}
		out.flush();
	}
	
	private int get_buffnum(int blocksize, int nblocks){
		if(nblocks < 3 && blocksize < 4){
			throw new RuntimeException("Not Enough Memory");
		}
		else if(nblocks * blocksize < 30){
			throw new RuntimeException("Not Enough Memory");
		}
		return (nblocks * blocksize) / 12;
	}
	
	private void asyncMerge(
			ExecutorService exe,
			final String in,
			final String out,
			final int blocksize,
			final int ways,
			final int start, 
			final int interval,
			final int mark
		){
		exe.execute(new Runnable(){
			@Override
			public void run() {
				try {
					merge(in,out,blocksize,ways,start,interval,mark);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	private void merge(
			String inpath,
			String outpath,
			int blocksize,
			int ways,
			int start, 
			int interval,
			int mark
		) throws IOException{
		
		//RandomAccessFile in = new RandomAccessFile(inpath, "r");
		RandomAccessFile out = new RandomAccessFile(outpath, "rw");
		out.seek(start);
		
		if(ways < 1){
			
			
		}else{
			int[] indexs = new int[ways];
			Arrays.fill(indexs, 0);
			boolean remain = false;
			do{
				DataInputStream in = DiskIO.open_input_run(inpath, blocksize);
				in.skipBytes(start);
				remain = false;
				int minIndex = 0;
				boolean first = true;
				for(int i = 0; i<ways; i++){
					if(indexs[i]<interval){
						try{
							in.skipBytes(indexs[i]);
							if(first){
								readUnit(outbuff,in);
								remain = true;
								minIndex = i;
								first = false;
							}
							else{
								readUnit(inbuff,in);
								remain = true;
								if(inbuff.compareTo(outbuff)<0){
									outbuff.setLeft(inbuff.getLeft());
									outbuff.setMiddle(inbuff.getMiddle());
									outbuff.setRight(inbuff.getRight());
									minIndex = i;
								}
							}
							in.skipBytes(interval-12-indexs[i]);
						}catch(Exception e){
						}
					}else{
						in.skipBytes(interval);
					}
				}
				if(remain){
					indexs[minIndex] += 12;
					append(out,outbuff,mark);
				}
				in.close();
			}while(remain);
			indexs = null;
			System.gc();
		}
		
		out.close();
	}
	
	public void readUnit(MutableTriple<Integer, Integer, Integer> triple, DataInput in) throws EOFException,IOException{
		triple.setLeft(in.readInt());
		triple.setMiddle(in.readInt());
		triple.setRight(in.readInt());
	}
	
	public int read(DataInput in, int limit, List<MutableTriple<Integer, Integer, Integer>> arr) throws IOException {
		int i = 0;
		try{
			for(; i<limit; i++){
				readUnit(arr.get(i),in);
			}
		}catch(EOFException e){
		}
		return i;
	}
	
	public void append(DataOutput out, List<MutableTriple<Integer, Integer, Integer>> arr, int records) throws IOException{
		for (int i=0; i<records; i++) {
			append(out,arr.get(i),0);
		}
	}
	
	public void append(DataOutput out, MutableTriple<Integer, Integer, Integer> elem, int mark) throws IOException{
		System.out.println("SET : " + elem + "and mark : " + mark);
		out.writeInt(elem.getLeft());
		out.writeInt(elem.getMiddle());
		out.writeInt(elem.getRight());
	}
	
	public void mkdir(String dir){
		File f = new File(dir);
		if(!f.exists()){
			f.mkdirs();
		}
		f = null;
		System.gc();
	}
	
	public int ceil_div(int x, int y) {
	    return (int)(x / y) + ((x % y > 0)?1:0);
	}
	
	public int ceil_div(double x, double y) {
	    return (int)(x / y) + ((x % y > 0)?1:0);
	}
	

	
}