package edu.hanyang.submit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.tartarus.snowball.ext.PorterStemmer;

import edu.hanyang.indexer.Tokenizer;

public class TinySETokenizer implements Tokenizer {
	
	private SimpleAnalyzer analyzer;
	private PorterStemmer stemmer;
	
	public void setup() {
		analyzer = new SimpleAnalyzer();
		stemmer = new PorterStemmer();
	}

	public List<String> split(String text) {
		List<String> list = new ArrayList<String>();
		TokenStream ts = analyzer.tokenStream("tk", text);
		try {
			ts.reset();
			while(ts.incrementToken()){
				CharTermAttribute ct = ts.addAttribute(CharTermAttribute.class);
				stemmer.setCurrent(ct.toString());
				stemmer.stem();
				list.add(stemmer.getCurrent());
				ts.end();
			}
			ts.close();
		} catch (IOException e) {
			
		}
		return list;
	}

	public void clean() {
		analyzer.close();
	}

}