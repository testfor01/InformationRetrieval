package edu.hanyang.submit;
 
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.apache.commons.lang3.tuple.MutableTriple;

import edu.hanyang.utils.DiskIO;
import edu.hanyang.indexer.ExternalSort;
 
public class TinySEExternalSort implements ExternalSort {
	
	public static String tmpfile = "/tmp";
	

	
	private MutableTriple<Integer, Integer, Integer> in_buff;
	private MutableTriple<Integer, Integer, Integer> out_buff;
	private ArrayList<MutableTriple<Integer, Integer, Integer>> memory;
	
	private DataInputStream bsin;
	private DataOutputStream bsout;
	
	
	
	public TinySEExternalSort(){
		in_buff = new MutableTriple<Integer, Integer, Integer>(0,0,0);
		out_buff = new MutableTriple<Integer, Integer, Integer>(0,0,0);
	}
	
	public void sort(String infile, String outfile, String tmpdir, int blocksize, int nblocks) throws IOException {
		// complete the method
		System.out.println("SORT START");
		mkdir(tmpdir);
		int memory_num = get_buffnum(blocksize, nblocks);
		
		memory = new ArrayList<MutableTriple<Integer, Integer, Integer>>(memory_num);
		System.out.println(memory_num);
		for(int i=0; i<memory_num; i++)
			memory.add(new MutableTriple<Integer, Integer, Integer>(0,0,0));
		bsin = DiskIO.open_input_run(infile, blocksize);
		int total = bsin.available()/12;
		int max_way = (blocksize*nblocks - 48)/(blocksize+4);
		int runs = ceil_div(total,memory_num);
		

		if(total == Integer.MAX_VALUE){
			throw new EOFException();
		}
		if(runs > 1)
			init_sort(bsin, tmpdir+tmpfile, memory, memory_num, blocksize,true);
		else
			init_sort(bsin, outfile, memory, memory_num, blocksize, false);
		bsin.close();
		memory = null;
		
		System.out.println("TOTAL INITIAL RUNS : " + runs);
		System.out.println("AVAILABLE MAX_WAYS : " + max_way + " BY MEMORY SPACE");
		if(max_way > runs){
			max_way = runs;
		}
		System.out.println("MERGE SORT OPTIONS : " + max_way + "-way merge sort at least");
		
		int remains = 0;
		int cnt;
		while(runs>1){
			remains = (runs % max_way == 0)?max_way:runs%max_way;
			runs = ceil_div(runs,max_way);
			
			for(cnt=0; cnt<runs; cnt++){
				bsout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outfile),blocksize));
				if(cnt == runs-1){
					merge(tmpdir+tmpfile,blocksize,bsout,remains,in_buff,out_buff,cnt*max_way);
				}else{
					merge(tmpdir+tmpfile,blocksize,bsout,max_way,in_buff,out_buff,cnt*max_way);
				}
				bsout.close();
				if(runs > 1){
					File target = new File(tmpdir+tmpfile+cnt);
					File outf = new File(outfile);
					outf.renameTo(target);
					target = null;
					outf = null;
					
				}
			}
		}
		
	}
	
	private int init_sort(DataInputStream in, String outpath, ArrayList<MutableTriple<Integer, Integer, Integer>> memory, int memory_num, int blocksize, boolean numbering) throws IOException{
		int count, runs = 0;
		
		DataOutputStream out;
		while((count = read(in, memory_num, memory)) > 0){
			String path = outpath;
			if(numbering){
				path += runs;
			}
			out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path),blocksize));
			Collections.sort(memory.subList(0, count));
			try{
				append(out, memory, count);
			}catch(EOFException e){
			}
			out.flush();
			out.close();
			runs++;
		}
		return runs;
	}
	
	private int get_buffnum(int blocksize, int nblocks){
		if(nblocks < 5 && blocksize < 1){
			throw new RuntimeException("Not Enough Memory");
		}
		else if(nblocks * blocksize < 56){
			throw new RuntimeException("Not Enough Memory");
		}
		return (nblocks * blocksize) / 12;
	}
	
	private static void merge(String path, int blocksize, DataOutputStream out, int ways, MutableTriple<Integer, Integer, Integer> in_buff, MutableTriple<Integer, Integer, Integer> out_buff, int start) throws IOException{
		boolean remain = false;
		DataInputStream[] in = new DataInputStream[ways];
		int setIndex = 0;
		for(int i=0; i<ways; i++){
			in[i] = DiskIO.open_input_run(path+(i+start), blocksize);
		}
		do{
			remain = false;
			boolean first = true;
			for(int i = 0; i<ways; i++){
				try{
					if(first){
						in[i].mark(12);
						readUnit(out_buff,in[i]);
						in[i].reset();
						remain = true;
						first = false;
						setIndex = i;
					}
					else{
						in[i].mark(12);
						readUnit(in_buff,in[i]);
						in[i].reset();
						remain = true;
						if(in_buff.compareTo(out_buff)<0){
							out_buff.setLeft(in_buff.getLeft());
							out_buff.setMiddle(in_buff.getMiddle());
							out_buff.setRight(in_buff.getRight());
							setIndex = i;
						}
					}
				}catch(EOFException e){
					in[i].close();
				}catch(IOException e){
				}
			}
			if(remain){
				append(out,out_buff);
				in[setIndex].skipBytes(12);
			}
		}while(remain);
		File f;
		for(int i=0; i<ways; i++){
			f = new File(path+(i+start));
			f.delete();
		}
		f = null;
		
	}
	
	public static void readUnit(MutableTriple<Integer, Integer, Integer> triple, DataInputStream in) throws EOFException,IOException{
		triple.setLeft(in.readInt());
		triple.setMiddle(in.readInt());
		triple.setRight(in.readInt());
	}
	
	public static int read(DataInputStream in, int limit, ArrayList<MutableTriple<Integer, Integer, Integer>> arr) throws IOException {
		int i = 0;
		try{
			for(; i<limit; i++){
				readUnit(arr.get(i),in);
			}
		}catch(EOFException e){
		}
		return i;
	}
	
	public static void append(DataOutputStream out, ArrayList<MutableTriple<Integer, Integer, Integer>> arr, int records) throws IOException{
		for (int i=0; i<records; i++) {
			append(out,arr.get(i));
		}
	}
	
	public static void append(DataOutputStream out, MutableTriple<Integer, Integer, Integer> elem) throws IOException{
		out.writeInt(elem.getLeft());
		out.writeInt(elem.getMiddle());
		out.writeInt(elem.getRight());
	}
	
	public static void mkdir(String dir){
		File f = new File(dir);
		if(!f.exists()){
			f.mkdirs();
		}
	}
	
	public static int ceil_div(int x, int y) {
	    return (int)(x / y) + ((x % y > 0)?1:0);
	}
	

	
}
