package edu.hanyang.util;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.util.Arrays;

public class StaticExternalBPTreeNode extends ExternalBNode{
	
	private int splitedSize = -1;
	private boolean isSplited = false;
	
	
	public StaticExternalBPTreeNode(int nodeSize, int depth, int order, boolean isLeaf) {
		super(nodeSize, depth, order, isLeaf);
	}

	public StaticExternalBPTreeNode(int nodeSize, int depth, int order) {
		super(nodeSize, depth, order);
	}
	
	@Override
	public String toString() {
		String s = "[";
		for(int i=0; i<size; i++){
			s = s.concat("("+keys[i] + "," + values[i] + ")" + ","); 
		}
		
		s = s.concat("end]");
		return s;
	}

	public void insert_unit(int key, int value) {
		int r = Arrays.binarySearch(keys,0,size++, key);
		System.arraycopy(keys, -r-1, keys, -r, size+r);
		System.arraycopy(values, -r-1, values, -r, size+r);
		keys[-r-1] = key;
		values[-r-1] = value;
//		
//		for(int i=size-1; i>=0 && key < keys[i]; i--){
//			swap(i,i+1);
//		}
	}

	@Override
	// 다음 iteration때 1 Node 기반으로 변경
	public void insert(int key, int value) {
		if(size < keys.length){
			isSplited = false;
			if(isLeaf){
				keys[size] = key;
				values[size++] = value;
			}else
				insert_unit(key,value);
			
		}else{
			//오른쪽에 삽입할 가장 큰 key 선정.
			hsort();
			isSplited = true;
			lastKey = keys[size-1];
			if( lastKey < key){
				lastKey = key;
				lastValue = value;
			}else{
				lastValue = values[--size];
				insert_unit(key,value);
			}

		}
	}
	
	public void writeWithSplited(SeekableByteChannel out, ByteBuffer b) throws IOException{
		hsort();
		int s = StaticExternalBPTreeNode.ceil_div(size+1, 2);
		splitedSize = size+1-s;
		this.size = s;
		write(out, b);
		write(out, b, this.size, splitedSize-1, true);
	}

	@Override
	public void delete(BNode from, int key, int value) {
	}

	@Override
	public int get(int key) throws NodeDataNotFound, IOException {
		return get(key, 1, null);
	}
	
	public int get(int key, int lastOrder, StaticExternalBPTree tree) throws NodeDataNotFound, IOException {
		int i = Arrays.binarySearch(keys, 0, size, key);
		boolean isLastOrder = lastOrder-1==order;
		if(i>=0){
			return values[i];
		}else{
			if(isLeaf){
				throw new NodeDataNotFound(0, depth, order, isLastOrder);
			}else{
				if(!(isLastOrder) && -i-1 == size){
					throw new NodeDataNotFound(
							1,
							depth, 
							getNextOrder(keys.length,depth, order, lastOrder, tree),
							order+1 == lastOrder-1
					);
				}
				else	
					throw new NodeDataNotFound(1,depth+1, getLeftOrder(keys.length,order,-i-1),isLastOrder && (-i-1 == size));
			}
		}
	}
	
	public static int getIndex(int nodeSize, int depth, int order){
		int result = order;
		for(int i=1; depth >0; i=i*nodeSize+1){
			result += i;
			depth--;
		}
		return result;
	}
	
	public static int getDepth(int nodeSize, int index){
		int depth = 0;
		int count = 0;
		for(int i=1; (index-1)/i > 0; i=i*nodeSize+1){
			count += i;
			if(i/count < 1)
				break;
			depth++;
		}
		return depth;
	}
	
	public static int getMaxNodeCount(int nodeSize, int depth){
		return BigInteger.valueOf(nodeSize).pow(depth+1).subtract(BigInteger.ONE).divide(BigInteger.valueOf(nodeSize-1)).intValue();
	}
		
	private void swap(int i, int j){
		int temp;
		temp = keys[i];
		keys[i] = keys[j];
		keys[j] = temp;
		temp = values[i];
		values[i] = values[j];
		values[j] = temp;
	}
	
	public static int ceil_div(int x, int y) {
	    return (int)(x / y) + ((x % y > 0)?1:0);
	}
	
	public static int getLeftOrder(int nodeSize, int order, int index){
		return (nodeSize) * order + index;
	}
	
	//파일구조로, skip수치를 기록하는 것은 어떨까?
	public static int getNextOrder(int nodeSize, int depth, int order, int lastOrder, StaticExternalBPTree tree) throws IOException{
		if(order == lastOrder-1){
			return -1;
		}else{
			int p0 = getParentOrder(nodeSize, order, lastOrder-1==order);
			int parentSize = tree.readSizeFromNode(depth-1, p0);
			if(order % nodeSize < parentSize-1)
				return order+1;
			else{
				int parentLastOrder = tree.getParentLastOrder(depth,lastOrder);
				int parentOrder = getParentOrder(nodeSize, order, lastOrder-1==order);
				if(parentOrder == parentLastOrder-1)
					return order+1;
				else
					return getLeftOrder(
							nodeSize,getNextOrder(
									nodeSize,
									depth-1,
									parentOrder,
									parentLastOrder,
									tree),
									0
							);
			}
		}
	}
	
	//파일구조로, skip수치를 기록하는 것은 어떨까?
	public static int getPrevOrder(int nodeSize, int depth, int order, int lastOrder, StaticExternalBPTree tree) throws IOException{
		if(order == 0){
			return -1;
		}else{
			if(lastOrder-1==order || order % nodeSize > 0)
				return order-1;
			else{
				int parentPrevNodeOrder = getPrevOrder(nodeSize,depth-1,getParentOrder(nodeSize, order, lastOrder-1==order), tree.getParentLastOrder(depth,lastOrder),tree);
				int parentPrevSize = tree.readSizeFromNode(depth-1, parentPrevNodeOrder);
				return getLeftOrder(nodeSize,parentPrevNodeOrder,parentPrevSize-1);
			}
		}
	}
	
	public static int getParentOrder(int nodeSize, int order, boolean isLastOrder){
		return (order- ((isLastOrder)?1:0)) / nodeSize ;
	}
	
	public boolean splited(){
		return isSplited;
	}
	
	public void clearSplited(){
		isSplited = false;
	}
	
    public void hsort()
    {
        for (int i = size/2 - 1; i >= 0; i--)
            heapify(size, i);
        for (int i=size-1; i>=0; i--){
        	swap(0,i);
            heapify(i, 0);
        }
    }
 
    private void heapify(int n, int i){
    	while(true){
    		int largest = i;
            int l = 2*i + 1;
            int r = 2*i + 2;
            if (l < n && keys[l] > keys[largest])
                largest = l;
            if (r < n && keys[r] > keys[largest])
                largest = r;
            if (largest != i){
            	swap(largest,i);
                i = largest;
            }else{
            	break;
            }
    	}
    }
	
}
