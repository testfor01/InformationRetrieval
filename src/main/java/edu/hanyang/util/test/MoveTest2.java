package edu.hanyang.util.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import edu.hanyang.util.StaticExternalBPTree;

public class MoveTest2 {

	public static int nodeSize = 3;
	public static void main(String[] args) throws FileNotFoundException, IOException {
		File file = new File("./test");
		File filemeta = new File("./testmeta");

		CleanTest.main(new String[2]);
		StaticExternalBPTree bptree = new StaticExternalBPTree(nodeSize,file,filemeta);
		

		bptree.familyInsert(19, 11);
		bptree.show();
		bptree.familyInsert(6, 11);
		bptree.show();
		bptree.familyInsert(17, 11);
		bptree.show();
		bptree.familyInsert(15, 11);
		bptree.show();
		bptree.familyInsert(13, 11);
		bptree.show();
		bptree.familyInsert(14, 11);
		bptree.show();
		bptree.familyInsert(5, 11);
		bptree.show();
		bptree.familyInsert(2, 11);
		bptree.show();
		bptree.familyInsert(16, 11);
		bptree.show();
		bptree.familyInsert(11, 11);
		bptree.show();
		bptree.familyInsert(7, 11);
		bptree.show();
		bptree.familyInsert(8, 11);
		bptree.show();
		bptree.familyInsert(0, 11);
		bptree.show();
		bptree.familyInsert(3, 11);
		bptree.show();
		bptree.familyInsert(4, 11);
		bptree.show();
		bptree.familyInsert(1, 11);
		bptree.show();
		bptree.familyInsert(10, 11);
		bptree.show();
		bptree.familyInsert(18, 11);
		bptree.show();
		bptree.familyInsert(12, 11);
		bptree.show();
		bptree.familyInsert(9, 11);
		bptree.show();
		
		bptree.close();
		

	}

}
