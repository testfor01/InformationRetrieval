package edu.hanyang.util.test;

import static org.junit.Assert.assertEquals;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.hanyang.util.NodeDataNotFound;
import edu.hanyang.util.StaticExternalBPTree;

public class UnitTest {
	
	static int blocksize = 4096;
	static int nblock = 4096;
	
	static long memory = blocksize*nblock;
	static long penalty = (int) (((memory/8 < 4)?4:memory/8) + ((memory>32 && memory<64)?memory/32:0));
	static int nodeSize = (int)(((memory-penalty)*45000)/1048576);
	
	@Before
	public void init() {
		CleanTest.main(new String[2]);
	}
	
	@Test
	public void myTest() throws IOException, NodeDataNotFound {
		
		System.out.println("pan-out : " + nodeSize);
		DataInputStream d = new DataInputStream(new FileInputStream("./treetest-15000000.data"));
		
		File file = new File("./test");
		File filemeta = new File("./testmeta");
		
		StaticExternalBPTree bptree = new StaticExternalBPTree(nodeSize, file, filemeta);
		
		long timestamp = System.currentTimeMillis();
		while(d.available() > 0){
			bptree.familyInsert(d.readInt(), d.readInt());
		}
		System.out.println("time duration for insertion: " + (System.currentTimeMillis() - timestamp) + " msecs");
		d.close();
//		bptree.close();
		
		
//		bptree = new StaticExternalBPTree(nodeSize, file);
		d = new DataInputStream(new FileInputStream("./treetest-15000000.data"));
		
		timestamp = System.currentTimeMillis();
		while(d.available() > 0){
			int v = bptree.get(d.readInt());
			assertEquals(v,d.readInt());
		}
		System.out.println("time duration for insertion: " + (System.currentTimeMillis() - timestamp) + " msecs");
		d.close();
		bptree.close();
	}

}
