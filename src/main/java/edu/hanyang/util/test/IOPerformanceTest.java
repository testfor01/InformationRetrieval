package edu.hanyang.util.test;


import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.StandardOpenOption;

import org.junit.Before;
import org.junit.Test;

import edu.hanyang.util.NodeDataNotFound;

public class IOPerformanceTest {
	public File file;
	public RandomAccessFile r;
	public DataInputStream d;
	public SeekableByteChannel s;
	public ByteBuffer buffer;
	
	@Before
	public void init() throws IOException {
		file = new File("./treetest-15000000.data");
		d = new DataInputStream(new FileInputStream(file));
		r = new RandomAccessFile(file,"r");
		s = FileChannel.open(file.toPath(), StandardOpenOption.READ);
		
	}
	
	@Test
	public void myTest() throws IOException, NodeDataNotFound {
//		buffer.allocateDirect(1024);
//		d.skip(file.length()-8);
//		System.out.println(d.read());
//		r.seek(file.length()-4);
//		r.readInt();
//		s.position(file.length()-4);
	}
	
	public void close() throws IOException, NodeDataNotFound {
		d.close();
		r.close();
		s.close();
	}
	
}
