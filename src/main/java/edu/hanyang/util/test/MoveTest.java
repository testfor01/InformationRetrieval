package edu.hanyang.util.test;

import java.io.File;
import java.io.IOException;

import edu.hanyang.util.StaticExternalBPTree;
import edu.hanyang.util.NodeDataNotFound;

public class MoveTest {
	static int nodeSize = 161;

	public static void main(String[] args) throws IOException, NodeDataNotFound {
		File file = new File("./test");
		File filemeta = new File("./testmeta");

		CleanTest.main(new String[2]);
		StaticExternalBPTree bptree = new StaticExternalBPTree(nodeSize,file,filemeta);
		
		//bptree.show();
		bptree.insert(3, 11);
		bptree.show();
		bptree.insert(8, 77);
		bptree.show();
		bptree.insert(2, 5);
		bptree.show();
		bptree.insert(1, 20);
		bptree.show();
		bptree.insert(70, 32);
		bptree.show();
		bptree.insert(6, 6);
		bptree.show();
		bptree.insert(62, 1);
		bptree.show();
		bptree.insert(11, 7);
		bptree.show();
		bptree.insert(28, 8);
		bptree.show();
		bptree.insert(104, 7);
		bptree.show();
		bptree.insert(7, 100);
		bptree.show();
		bptree.insert(61, 52);
		bptree.show();
		bptree.insert(55, 764);
		bptree.show();
		bptree.insert(53, 1028);
		bptree.show();
		bptree.insert(162, 3333);
		bptree.show();
		bptree.insert(300, 5020);
		bptree.show();
		bptree.insert(4, 7415);
		bptree.show();
		bptree.insert(42, 88282);
		bptree.show();
		bptree.insert(9, 71);
		bptree.show();
		bptree.insert(161, 3333);
		bptree.show();
		bptree.insert(39, 5020);
		bptree.show();
		bptree.insert(17, 7415);
		bptree.show();
		bptree.insert(20, 88282);
		bptree.show();
		bptree.insert(5, 71);
		bptree.show();
		bptree.insert(35, 10);
		bptree.show();
		bptree.insert(12, 4);
		bptree.show();
		bptree.insert(111, 52);
		bptree.show();
		bptree.insert(69, 100);
		bptree.show();
		bptree.insert(80, 8);
		bptree.show();
		bptree.insert(188, 14);
		bptree.show();
		bptree.insert(10, 30);
		bptree.show();
		bptree.insert(121, 220);
		bptree.show();
		bptree.insert(47, 57);
		bptree.show();
		bptree.insert(22, 71);
		bptree.show();
		bptree.insert(99, 70);
		bptree.show();
		bptree.insert(29, 46);
		bptree.show();
		bptree.insert(76, 5);
		bptree.show();
		bptree.insert(211, 400);
		bptree.show();
		bptree.close();

	}

}
