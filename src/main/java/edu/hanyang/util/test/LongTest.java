package edu.hanyang.util.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import edu.hanyang.util.NodeDataNotFound;
import edu.hanyang.util.StaticExternalBPTree;

public class LongTest {
	static int nodeSize = 1800;
	static int count = 100000;
	
	@Before
	public void init() {
		CleanTest.main(new String[2]);
	}
	
	@Test
	public void myTest() throws IOException, NodeDataNotFound {
		File file = new File("./test");
		File filemeta = new File("./testmeta");
		StaticExternalBPTree bptree = new StaticExternalBPTree(nodeSize, file, filemeta);
		
		ArrayList<Integer> c = new ArrayList<Integer>(count);
		int i = 0;
		for(i=0; i<count; i++){
			c.add(i);
		}
		Collections.shuffle(c);
		
		long timestamp = System.currentTimeMillis();
		try{
			for(i=0; i<count; i++){
				int r = c.get(i);
//				System.out.println("input : " + r);
				bptree.familyInsert(r, i);
//				bptree.show();
//				for(int j=0; j<i; j++)
//					bptree.get(c.get(j));
				
			}
		bptree.familyInsert(2147483647, 8888);
		System.out.println("time duration for insertion: " + (System.currentTimeMillis() - timestamp) + " msecs");
		//bptree.close();
			
			
			
			System.out.println("삽입끝 bp 횟수 : " + bptree.bpcount);
			System.out.println("leafDepth : " + bptree.leafDepth);
//			bptree.show();
			System.out.println("총 개수 : " + i);
			timestamp = System.currentTimeMillis();
			for(int j=0; j<i/100; j++){
				int r = c.get(j);
//				System.out.println("검색: " + r);
				bptree.get(r);
			}
			bptree.get(2147483647);
			System.out.println("time duration for insertion: " + (System.currentTimeMillis() - timestamp) + " msecs");
			bptree.close();
		}catch(RuntimeException e){
			bptree.show();
			System.out.println("에러발생원인 :  " + (c.get(i)));
			e.printStackTrace();
		}
//		catch(NodeDataNotFound e){
//			System.out.println("무결성검사 실패, 카운트 : " + (i+1));
//			bptree.show();
//		}finally{
//			bptree.close();
//		}
		
		
	}

}
