package edu.hanyang.util.test;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;

import edu.hanyang.util.StaticExternalBPTreeNode;

import java.nio.ByteBuffer;

public class BPTreeRWTest{

	public static void main(String[] args) throws FileNotFoundException, IOException {
		File file = new File("./test");
		file.createNewFile();
		FileChannel in = FileChannel.open(file.toPath(), StandardOpenOption.READ);
		FileChannel out= FileChannel.open(file.toPath(), StandardOpenOption.WRITE);
		ByteBuffer buffer = ByteBuffer.allocate(16);
		StaticExternalBPTreeNode b0 = new StaticExternalBPTreeNode(5,0,0);
//		StaticExternalBPTreeNode b1 = new StaticExternalBPTreeNode(5,1,0);
//		StaticExternalBPTreeNode b2 = new StaticExternalBPTreeNode(5,1,1);
//		StaticExternalBPTreeNode b3 = new StaticExternalBPTreeNode(5,1,2);
//		StaticExternalBPTreeNode b4 = new StaticExternalBPTreeNode(5,1,3);
//		StaticExternalBPTreeNode b5 = new StaticExternalBPTreeNode(5,1,4);
//		StaticExternalBPTreeNode b6 = new StaticExternalBPTreeNode(5,1,5);
		
		b0.insert_unit(2, 1);
		b0.insert_unit(7, 22);
		b0.insert_unit(1, 333);
		b0.insert_unit(8, 4444);
		b0.insert_unit(9, 55555);
		
//		b0.keys[0] = 3;
//		b0.values[0] = 6;
//		b0.keys[1] = 1;
//		b0.values[1] = 8;
//		b0.size = 2;
//		
//		b5.keys[0] = 8;
//		b5.values[0] = 8;
//		b5.keys[1] = 9;
//		b5.values[1] = 2;
//		b5.keys[2] = 77;
//		b5.values[2] = 311;
//		b5.size = 3;

		b0.write(out, buffer);
//		b1.write(out, buffer);
//		b2.write(out, buffer);
//		b3.write(out, buffer);
//		b4.write(out, buffer);
//		b5.write(out, buffer);
//		b6.write(out, buffer);
		
//		b0.read(in, buffer);
//		b1.read(in, buffer);
//		b2.read(in, buffer);
//		b3.read(in, buffer);
//		b4.read(in, buffer);
//		b5.read(in, buffer);
//		b6.read(in, buffer);
//		
		System.out.println(b0);
//		System.out.println(b1);
//		System.out.println(b2);
//		System.out.println(b3);
//		System.out.println(b4);
//		System.out.println(b5);
//		System.out.println(b6);

	}

}
