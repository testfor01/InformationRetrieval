package edu.hanyang.util;

import java.nio.file.StandardOpenOption;
import java.util.PriorityQueue;
import java.util.Queue;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.LinkedBlockingQueue;

public class StaticExternalBPTree implements BPTree, Closeable{

	private FileChannel in;
	private FileChannel out;
	private StaticExternalBPTreeNode node;
	private int nodeSize;
	private File meta;
	private ByteBuffer buffer;
	public int leafDepth;
	
	public int keyCache = -1;
	public int valueCache = -1;
	public int depthCache = -1;
	public int orderCache = -1;
	public int bufferUpperBound = -1;
	public int bufferLowerBound = -1;
	
	public PriorityQueue<Pair<Integer,Integer>> pq;
	
	public int maxPQSize;
	
	public static int bpcount = 0;
	
	private boolean depthSaturated;
	
	private boolean bufferOutdated = false;
	
	public StaticExternalBPTree(int nodeSize, File file, File metafile) throws FileNotFoundException, IOException{
		boolean isnew = file.createNewFile();
		in = FileChannel.open(file.toPath(), StandardOpenOption.READ);
		out= FileChannel.open(file.toPath(), StandardOpenOption.WRITE);
		this.meta = metafile;
		if(isnew){
			if(nodeSize <2){
				throw new RuntimeException("Opps, 'nodeSize' must be 2 at least");
			}
			this.nodeSize = nodeSize;
			leafDepth = 0;
			this.buffer = ByteBuffer.allocate(getNodeBytes());
			node = new StaticExternalBPTreeNode(nodeSize,0,0,true);
			node.write(out, buffer);
		}else{
			if(metafile.exists()){
				DataInputStream din = new DataInputStream(new FileInputStream(metafile));
				long fileSize = din.readLong();
				this.nodeSize = din.readInt();
				this.leafDepth= din.readInt();
				din.close();
				din = null;
				if(fileSize != file.length()){
					throw new RuntimeException("metafile is not valid.");
				}
				this.buffer = ByteBuffer.allocate(getNodeBytes());
				node = new StaticExternalBPTreeNode(this.nodeSize,0,0,true);
				getBPTreeNode(0,0,1);
			}else{
				throw new RuntimeException("metafile does not exist. Check if the path of metafile is incorrect. or Delete the b+tree file to create new b+tree storage. \"Did you close the B+Tree instance after insertion operation?\"");
			}
		}
		maxPQSize = (nodeSize/10 < 10)?10:nodeSize/10;
		pq = new PriorityQueue<Pair<Integer,Integer>>(maxPQSize);
	}
	
	public StaticExternalBPTree(File file, File metafile) throws FileNotFoundException, IOException{
		this(0, file, metafile);
	}
	
	private int getLeafMaxOrder() throws IOException{
		int maxOrder = 0;
		int depth = 0;
		int size = 0;
		while(depth < leafDepth){
			size = readSizeFromNode(depth++, maxOrder);
			maxOrder = StaticExternalBPTreeNode.getLeftOrder(nodeSize, maxOrder, size-1)+1;
		}
		return maxOrder;
	}
	
	private void resetInput() throws IOException{
		resetOutput(0,0);
	}
	
	private long resetInput(int depth, int order) throws IOException{
		return in.position(getNodeBytes()*StaticExternalBPTreeNode.getIndex(nodeSize, depth, order)).position();
	}
	
	private long resetOutput(int depth, int order) throws IOException{
		return out.position(getNodeBytes()*StaticExternalBPTreeNode.getIndex(nodeSize, depth, order)).position();
	}
	
	public int getNodeBytes(){
		return nodeSize * 8 + 4;
	}

	
	/**		
	 * node와 splittedNode를 이용해서 해당 트리를 재구성한다.
	 *  
	 * @param depth 트리의 깊이
	 * @param lastOrder 해당 depth에서의 마지막 노드의 위치
	 * @param nodeMaxCount 해당 depth에서의 이론적 최대 노드 개수 
	 * @param parentSize 부모 노드의 사이즈
	 * @param splitedNode 현재 노드의 split복제본
	 * @throws IOException
	 */
	public void copy(int depth, int lastOrder, int nodeMaxCount, int parentSize) throws IOException{
//		 (1). depth 이동조건일시,
//		    1. 현재 depth의 모든 노드를 전부를 depth+1로 이동시킨다.
//		    2. 부모노드 역시 이동 전 depth로 내려올 것이므로,미리 언벨런싱 징후를 포착해 hardShift으로 리벨런싱한다.
//		    3. leafDepth였다면, 최대 leafDepth += 1 한다.
//		 (2). depth 이동조건이 아닐시,
//		    (2.1). 부모가 포화면,
//		       1. 현재 노드 다음칸 부터 lastOrder-(order+1)칸 만큼 hardShift한다.
//		       2. Split을 현재 노드의 오른쪽 쓴다.
//		       3. 부모 노드가 쪼개진 이후의 상황에 맞게 hardShift으로 리벨런싱한다.
//		    (2.2). 부모가 비포화면,
//		     	 1. 현재 노드 다음 칸 부터 (부모노드의 갯수-1)- 부모기준 node의 상대인덱스 ( 부모가 Last인 경우 +1칸 ) 만큼 hardShift한다.
//			   	 2. Split을 현재 노드의 오른쪽 쓴다.
//               3. lastOrder가 밀려나지 않았을 경우, (2.1)과 (1)에서 부모노드가 쪼개진 이후를 대비해 쉬프트한걸 undo한다.
//      특별한 상황, split이 일어났지만 벨런싱이 필요없는 상황 : 쪼갬이 일어나는 경로중에 (2)에서 부모가 비포화이면서 동시에 lastOrder가 아니면, 삽입으로 인한 자식노드의 lastOrder가 증가하지 않는다. 이 경우 하위 depth역시 (2.1)과정에서 벨런싱이 일어나지 않는다.
		
		int g = node.getOrder();
		int k = (g==nodeMaxCount-1)?nodeSize:g % (nodeSize);
		//i 는 부모depth에서 split이 일어나는 장소에서 가장 왼쪽에 있는 자식노드의 인덱스이다.
		int i = StaticExternalBPTreeNode.getLeftOrder(nodeSize, StaticExternalBPTreeNode.getParentOrder(nodeSize, g, g==lastOrder-1), 0);

		// (1). depth 이동조건일시,
		if(depthSaturated){
			//1. 현재 depth 전부를 depth+1로 이동시킨다.
			resetInput(depth,0);
			resetOutput(depth+1,0);
			move(g);
			depthCache = depth+1;
			node.depth++;
//			skip(out,1);
			node.writeWithSplited(out, buffer);
			bufferUpperBound = node.keys[node.size];
			skip(in,1);
			move(lastOrder - (g+1));
			
			if(node.isLeaf){
				leafDepth++;
			}
			
			//2. 부모가 그냥 내려온 직후를 대비해 hardShift으로 리벨런싱한다.
			if(depth != 0){
				//int d = 0;
				int pLeft = getPSizeAfterSplit(true);
				int pRight= getPSizeAfterSplit(false);
				deepSaparate(depth+1,
						i+pLeft,
						nodeSize+i,
						lastOrder - (i+pLeft-1),
						lastOrder+1
				);
				deepSaparate(depth+1,
						i+nodeSize+pRight+(((i+nodeSize+pRight)/nodeSize==getParentLastOrder(depth+1,lastOrder))?1:0),
						//i+nodeSize+pRight의 부모가 LastOrder+1에 해당되게 되면, 덜 옮김.
						//부모depth의 lastOrder+1의 인덱스 = Parent(depth, lastOrder)+1-1
						//i+nodeSize+pRight의 부모노드의 인덱스 = (i+nodeSize+pRight-((isLast)?1:0)/nodeSize
						//i+nodeSize+pRight가 isLast이려면? -> 
						//  (1) 부모노드가 포화상태이다.
						//  (2) 이 노드의 오른쪽이 비어있지 않아야 한다.
						//  내려온 직후의 부모의 lastOrder+1노드는 항상 불포화노드이므로, isLast = false
						// 
						i+2*nodeSize,
						lastOrder-(i+nodeSize+pRight-1) + nodeSize - pLeft,
						lastOrder+1+ (nodeSize - pLeft)
				);
			}
			
		}
		// (2). depth 이동조건이 아닐시,
		else{
			//(2.1). 부모가 포화면,
			
			if(parentSize==nodeSize){
				shallowSaperate(depth,
						g+1,
						g+2,
						lastOrder - (g+1)
				);
				//2. Split을 현재 노드의 오른쪽 쓴다.
				resetOutput(depth,g);
				//skip(out,1);
				node.writeWithSplited(out, buffer);
				bufferUpperBound = node.keys[node.size];
				
				//3. 부모가 Split된 이후를 대비해 리벨런싱 한다.
				if(depth != 0){
					//int d = 0;
					int pLeft = getPSizeAfterSplit(true);
					int pRight= getPSizeAfterSplit(false);
					deepSaparate(depth,
							i+pLeft,
							nodeSize+i,
							lastOrder - (i+pLeft-1),
							lastOrder+1
					);
					deepSaparate(depth,
							i+nodeSize+pRight,
							//i+nodeSize+pRight의 부모가 LastOrder+1에 해당되면 한칸 덜 옮김.
							i+2*nodeSize-(((i+nodeSize+pRight)/nodeSize==(lastOrder+1)/nodeSize)?1:0),
							lastOrder-(i+nodeSize+pRight-1) + nodeSize - pLeft,
							lastOrder+1+ (nodeSize - pLeft)
					);
				}

			}
			//(2.2). 부모가 비포화면,
			else{
				//1. 현재 노드 다음 칸 부터 (부모노드의 갯수-1)- 부모기준 node의 상대인덱스 ( 부모가 LastOrder인 경우 +1칸 ) 만큼 hardShift한다.
				if(g < lastOrder-1){
					int moveCount = parentSize+(((g+1)/nodeSize==getParentLastOrder(depth,lastOrder)-1)?1:0)-(k+1);
					shallowSaperate(depth,
							g+1,
							g+2,
							moveCount
					);
				}
				//2. Split을 현재 노드의 오른쪽 쓴다.
				resetOutput(depth,g);
				//skip(out,1);
				node.writeWithSplited(out, buffer);
				bufferUpperBound = node.keys[node.size];
			}
			
		}
	}
	
	private int getPSizeAfterSplit(boolean isLeft){
		if(isLeft)
			return nodeSize+1 - (nodeSize+1)/2;
		else
			return (nodeSize+1)/2;
	}

	
	private void skip(FileChannel c, int index) throws IOException{
		c.position(c.position() + getNodeBytes()*index);
	}
	
	private void shallowSaperate(int depth, int order, int targetOrder, int unitCounts) throws IOException{
		if(targetOrder-order < unitCounts){
			shallowSaperate(depth, targetOrder, 2 * targetOrder - order, unitCounts - (targetOrder-order));
			unitCounts = targetOrder-order;
		}

		
		resetOutput(depth,targetOrder);
		resetInput(depth, order);
		move(unitCounts);
		
		
	}
	
	private void deepSaparate(int depth, int order, int targetOrder, int unitCounts, int lastOrder) throws IOException{
		if(unitCounts<0){
			throw new RuntimeException("Nagative size err.");
		}
		if(order >= targetOrder)
			return;
		int extra = (order+unitCounts == lastOrder)?1:0;
		do{
			if(order == targetOrder || unitCounts == 0)
				break;
			if(depth == leafDepth){
				long pos= resetInput(depth, order+unitCounts);
				long distance = in.size() - pos;
				if(distance < 0){
					unitCounts += distance/getNodeBytes();
				}
			}			
			shallowSaperate(depth,order,targetOrder,unitCounts);
			if(depth == leafDepth+1){
				break;
			}else{
				depth++;
				order = StaticExternalBPTreeNode.getLeftOrder(nodeSize, order, 0);
				targetOrder = StaticExternalBPTreeNode.getLeftOrder(nodeSize, targetOrder, 0);
				unitCounts *= nodeSize;
				unitCounts += extra;
			}
		}while(depth<leafDepth+1);
	}

	private void arrangeRootNode() throws IOException{
		resetOutput(0,0);
		if(node.size == nodeSize)
			node.write(out, buffer,0,0,true);
		else
			node.write(out, buffer,node.size,1,false);
	}
	
	int getParentLastOrder(int depth, int lastOrder){
		return StaticExternalBPTreeNode.ceil_div(lastOrder-1, nodeSize);
	}
	
	private void move(int unitCounts) throws IOException{
		if(unitCounts > 0){
			int remain = unitCounts * getNodeBytes();
			buffer.limit(buffer.capacity());
			while(remain > 0){
				if(remain < buffer.capacity()){
					buffer.limit(remain);
				}
				buffer.position(0);
				if(in.read(buffer) == 0)
					throw new EOFException();
				buffer.flip();
				remain -= out.write(buffer);
			}
		}
		
	}
	
	public void getBPTreeNode(int depth, int order, int lastOrder) throws IOException{
		if(depth != depthCache || order != orderCache){
			flush();
			depthCache = depth;
			orderCache = order;	
			node.depth = depth;
			node.order = order;
			node.isLeaf = depth==leafDepth;
			resetInput(depth,order);
			node.read(in,buffer);
			bufferLowerBound = node.keys[0];
			int nextOrder = StaticExternalBPTreeNode.getNextOrder(nodeSize, depth, order, lastOrder, this);
			if(depth == leafDepth){
				if(nextOrder == -1){
					bufferUpperBound = 2147483647;
				}else{
					readKeyValueFromNode(depth,nextOrder,0);
					bufferUpperBound = keyCache;
				}
//				System.out.println("(d,o) : " + depth + "," + order);
//				System.out.println("bound : " + bufferLowerBound + " to " + bufferUpperBound);
			}else{
				
			}
			
			
			bpcount++;
			//System.out.println("bp duration: " + (System.currentTimeMillis() - timestamp) + " msecs");
		}
	}
	
	public void getBPTreeNode(ExternalBNode node, int depth, int order) throws IOException{
		if(node == null)
			node = new StaticExternalBPTreeNode(nodeSize,0,0,true);
		node.depth = depth;
		node.order = order;
		node.isLeaf = depth==leafDepth;
		resetInput(depth,order);
		node.read(in,buffer);
	}
	
	public int readSizeFromNode(int depth, int order) throws IOException{
		if(depth == depthCache && order == orderCache){
			return node.size;
		}
		resetInput(depth,order);
		buffer.limit(4);
		buffer.position(0);
		in.read(buffer);
		buffer.flip();
		return buffer.getInt();
	}
	
	public void updateKeyValueFromNode(int depth, int order, int key, int value, int index) throws IOException{
		
		resetOutput(depth,order);
		buffer.limit(8);
		buffer.position(0);
		buffer.putInt(key);
		buffer.putInt(value);
		buffer.flip();
		out.position(out.position() + 4 + index*8);
		out.write(buffer);
		buffer.flip();
	}
	
	public void readKeyValueFromNode(int depth, int order, int index) throws IOException{
		if(depth == depthCache && order == orderCache){
			keyCache = node.keys[index];
			valueCache= node.values[index];
		}
		resetInput(depth,order);
		in.position(in.position() + 4 + index*8);
		buffer.limit(8);
		buffer.position(0);
		in.read(buffer);
		buffer.flip();
		keyCache = buffer.getInt();
		valueCache=buffer.getInt();
	}
	
	
	
	private int goToLeafLightweight(int key) throws IOException, NodeDataNotFound{
		resetInput();
		int depth = 0;
		int order = 0;
		int lastOrder = 1;
		while(true){
			getBPTreeNode(depth,order, lastOrder);	
			try{
				return node.get(key,lastOrder,this);
			}catch(NodeDataNotFound e){
				if(depth != e.depth){
					getBPTreeNode(e.depth-1,lastOrder-1, lastOrder);
					lastOrder = (lastOrder-1) * (nodeSize) + node.size+1;
				}
				switch(e.flag){
					case 0:
						throw e;
					case 1:
						depth = e.depth;
						order = e.order;
						break;
					default:
						throw new RuntimeException("unsupported flag : " + e.flag);
				}
			}	
		}
	}
	
	private int goToLeaf(int key) throws IOException, NodeDataNotFound{
		resetInput();
		int depth = 0;
		int order = 0;
		int lastOrder = 1;
		
		depthSaturated = true;
		while(true){
			getBPTreeNode(depth,order,lastOrder);	
			try{
				return node.get(key,lastOrder,this);
			}catch(NodeDataNotFound e){
				if(depth != e.depth){
					
					depthSaturated = depthSaturated && node.size == nodeSize;
					
					
					getBPTreeNode(e.depth-1,lastOrder-1,lastOrder);
					lastOrder = (lastOrder-1) * (nodeSize) + node.size+1;
					
				}
				switch(e.flag){
					case 0:
						throw e;
					case 1:
						depth = e.depth;
						order = e.order;
						break;
					default:
						throw new RuntimeException("unsupported flag : " + e.flag);
				}
			}	
		}
	}
	
	public void simulationForDepthSaturated(int key, int depth, int order, int lastOrder) throws IOException{
		depthSaturated = true;
		int size = 0;
		while(depth >= 0){
			size = readSizeFromNode(depth,order);
			depthSaturated = depthSaturated && size == nodeSize;
			order = StaticExternalBPTreeNode.getParentOrder(nodeSize, order, order==lastOrder-1);
			lastOrder = getParentLastOrder(depth,lastOrder);
			depth--;
		}
	}

	@Override
	public int get(int key) throws IOException, NodeDataNotFound{
		familyFlush();
		flush();
		if(depthCache == leafDepth && key>= bufferLowerBound && key< bufferUpperBound){
			return node.get(key);	
		}
		if(keyCache == key)
			return valueCache;
		return goToLeafLightweight(key);
	}
	
	private int insert(int depth, int order, int key, int value) throws IOException{
		int targetKey = key;
		int targetValue = value;
		int targetDepth = depth;
		int targetOrder = order;
		int parentSize = 0;
		int nodeMaxCount = StaticExternalBPTreeNode.getMaxNodeCount(nodeSize,targetDepth);
		//lastOrder를 insert마다 구하는게 아니라, 필드변수에 박아놓는 방법이 더 좋을 듯 하다.
		//lastOrder 수정.
		int lastOrder = getLeafMaxOrder()+1;
		
		simulationForDepthSaturated(targetKey, targetDepth, targetOrder, lastOrder);
		while(targetDepth >= 0){
			if(targetDepth > 0)
				parentSize = readSizeFromNode(targetDepth-1,StaticExternalBPTreeNode.getParentOrder(nodeSize, targetOrder, targetOrder==lastOrder-1));
			getBPTreeNode(targetDepth, targetOrder, lastOrder);
			node.insert(targetKey, targetValue);
			if(node.splited()){
				bufferOutdated = false;
				copy(targetDepth,lastOrder, nodeMaxCount,parentSize);
				if(targetOrder > 0 && targetKey == node.getKey(0)){
					nodeUpdateChain(targetDepth, targetOrder, targetKey, targetValue, 0, lastOrder);
				}
				targetOrder = StaticExternalBPTreeNode.getParentOrder(nodeSize, node.order, node.order==lastOrder-1);
				lastOrder = getParentLastOrder(targetDepth,lastOrder);
				if(node.size == nodeSize){
					targetKey = node.lastKey;
					targetValue=node.lastValue;
				}else{
					targetKey = node.keys[node.size];
					targetValue = node.values[node.size];
				}
				node.clearSplited();
				targetDepth--;
				if(targetDepth < 0){
					arrangeRootNode();
					break;
				}
				
			}else{
				bufferOutdated = true;
				if(lastOrder-1 != targetOrder){
				}
//				resetOutput(node.depth,node.order);
				if(targetOrder > 0 && targetKey == node.getKey(0)){
					nodeUpdateChain(targetDepth, targetOrder, targetKey, targetValue, 0, lastOrder);
				}
				getBPTreeNode(depth,order,getLeafMaxOrder()+1);
				return key;
			}
			nodeMaxCount = (nodeMaxCount-1)/nodeSize;
		}
		keyCache = key;
		valueCache=value;
		getBPTreeNode(depth,order,getLeafMaxOrder()+1);
		return key;
	}
	
	public void flush() throws IOException{
		if(bufferOutdated){
			node.hsort();
			resetOutput(depthCache,orderCache);
			node.write(out, buffer);
			bufferOutdated = false;
		}
	}
	
	public void familyInsert(int key, int value) throws IOException{
		pq.add(Pair.of(key, value));
		if(pq.size() >= this.maxPQSize){
			familyFlush();
		}
	}
	
	public void familyFlush() throws IOException{
		while(!pq.isEmpty()){
			Pair<Integer,Integer> kv = pq.remove();
			insert(kv.getKey(),kv.getValue());
		}
	}

	@Override
	public int insert(int key, int value) throws IOException {
		
		if(depthCache == leafDepth && key>= bufferLowerBound && key<= bufferUpperBound){
			return insert(depthCache,orderCache,key,value);	
		}
		try{
			int v = goToLeaf(key);
			throw new RuntimeException("Violation of unique key policy. [Duplicated key : (" + key + "," + v + ")]" );
		}catch(NodeDataNotFound e){
			return insert(e.depth,e.order,key,value);
		}
	}
	
	public int nodeUpdateChain(int fromDepth, int fromOrder, int key, int value, int checkIndex, int lastOrder) throws IOException{
		readKeyValueFromNode(fromDepth, fromOrder, checkIndex);
		if(key <= keyCache){
			keyCache = key;
			updateKeyValueFromNode(fromDepth, fromOrder, key, value,checkIndex);
			int fromPrev = StaticExternalBPTreeNode.getPrevOrder(nodeSize, fromDepth, fromOrder, lastOrder, this);
			if(fromPrev != -1){
				checkIndex = (fromPrev)%nodeSize;
				fromOrder = StaticExternalBPTreeNode.getParentOrder(nodeSize, fromPrev, fromPrev==lastOrder);
				return nodeUpdateChain(fromDepth-1, fromOrder, keyCache, valueCache, checkIndex, getParentLastOrder(fromDepth,lastOrder));
			}
		}
		return key;
	}

	@Override
	public int delete(int key){
		throw new RuntimeException("Not Yet");
	}
	
	@Override
	public int update(int key, int value){
		throw new RuntimeException("Not Yet");
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public int nodeSize() {
		return nodeSize;
	}

	public void show() throws IOException {
		familyFlush();
		flush();
		System.out.println("========================================");
		System.out.println("Total Depth : " + (leafDepth + 1));
		Queue<Triple<Integer,Integer,Boolean>> queue = new LinkedBlockingQueue<Triple<Integer,Integer,Boolean>>();
		queue.add(Triple.of(0, 0, true));
		while(!queue.isEmpty())
			show(new StaticExternalBPTreeNode(nodeSize,0,0,true), queue);
		System.out.println("========================================");
	}
	
	public void show(ExternalBNode node, Queue<Triple<Integer,Integer,Boolean>> queue) {
		Triple<Integer,Integer,Boolean> p = queue.remove();
		try {
			getBPTreeNode(node, p.getLeft(), p.getMiddle());
			System.out.println("(" + p.getLeft() + "," + p.getMiddle() + ") : " + node + " and leaf : " + node.isLeaf);
			int range = node.size;
			if(!node.isLeaf){
				for(int i=0; i<range; i++)
					queue.add(Triple.of(p.getLeft()+1, StaticExternalBPTreeNode.getLeftOrder(nodeSize, p.getMiddle(), i),false));
				if(p.getRight())
					queue.add(Triple.of(p.getLeft()+1, StaticExternalBPTreeNode.getLeftOrder(nodeSize, p.getMiddle(), node.size),true));
			}
		} catch (IOException e) {
		}
	}
	
	public void close() throws IOException{
		familyFlush();
		flush();
		DataOutputStream fout = new DataOutputStream(new FileOutputStream(meta));
		fout.writeLong(in.size());
		fout.writeInt(nodeSize);
		fout.writeInt(leafDepth);
		fout.close();
		fout = null;
		node = null;
		buffer = null;
		pq = null;
		in.close();
		out.close();
		in = null;
		out = null;
		System.gc();
		try {
			this.finalize();
		} catch (Throwable e) {
		}
	}
	
}
