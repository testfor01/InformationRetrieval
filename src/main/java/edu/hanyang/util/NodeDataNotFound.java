package edu.hanyang.util;

public class NodeDataNotFound extends Throwable {

	/**
	 * flag는 0일시 Nodata, 1일시 하위 노드 참조 신호.
	 * value는 flag가 0일시, 마지막으로 탐색시도한 노드 order, flag1일시, 참조 해야 할 노드 order 
	 */
	private static final long serialVersionUID = -9056375013429881771L;
	
	public int flag;
	
	public int depth;
	
	public int order;
	
	public boolean isLastOrder;

	public NodeDataNotFound(int flag, int depth, int order, boolean isLastOrder) {
		super("No data for the key in B+Tree");
		this.flag = flag;
		this.depth = depth;
		this.order = order;
		this.isLastOrder = isLastOrder;
	}

}
