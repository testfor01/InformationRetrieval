package edu.hanyang.unit.builder;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.lang3.tuple.MutableTriple;

import edu.hanyang.utils.DiskIO;

public class TestFileValidator {
	public static int blocksize = 12;
	public static int nblocks = 3;
	public static int unitcounts = 20;
	
	public static String val1 = "src/test/resources/test.data";
	public static String val2 = "src/test/resources/answer.data";
	public static String val3 = "sorted.data";
	
	public static void main(String[] args) throws IOException{
		ArrayList<MutableTriple<Integer,Integer,Integer>> trialUnit =
				new ArrayList<MutableTriple<Integer,Integer,Integer>>();
		for(int i=0; i<unitcounts;i++)
			trialUnit.add(new MutableTriple<Integer,Integer,Integer>(0,0,0));
		DiskIO.read_array(DiskIO.open_input_run(val3, blocksize), unitcounts, trialUnit);
		BuilderTester.printList(trialUnit, "Validator List");
	}

}
