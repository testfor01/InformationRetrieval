package edu.hanyang.unit.builder;

import java.io.File;
import java.util.List;

public interface Builder<T> {
	
	public List<T> createUnitTester();
	
	public File createFile(String path, List<T> in);

}
