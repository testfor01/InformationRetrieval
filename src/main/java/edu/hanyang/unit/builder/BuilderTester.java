package edu.hanyang.unit.builder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.tuple.MutableTriple;

import edu.hanyang.submit.TinySEExternalSort;
import edu.hanyang.utils.DiskIO;

import static org.junit.Assert.*;

import org.junit.Test;

public class BuilderTester {
	
	/*
	 * ===============================================
	 * FIX THIS STATIC PARAMETER
	 * ===============================================
	 */
	
	public static String initPath = "./tempIn.data";
	public static String trialPath = "./tempOut.data";
	public static String answerPath = "./tempAnswer.data";
	public static int blocksize = 4096;
	public static int nblocks = 1024;
	public static int unitcounts = 10000000;
	
	public static boolean unitView = false;
	public static boolean trialView = false;
	public static boolean answerView = false;
	
	/*
	 * ===============================================
	 * ===============================================
	 */

	@Test
	public void test() {
		MyTripletBuilder b = new MyTripletBuilder(unitcounts);
		List<MyTriplet> units1 = b.createUnitTester();
		
		// init
		List<MyTriplet> myUnit = (List<MyTriplet>)units1;
		b.createFile(initPath, myUnit);
		if(unitView)
			printList(myUnit,"Initial Test Units");
		
		// trial
		ArrayList<MutableTriple<Integer,Integer,Integer>> trialUnit = null;
		try {
			long timestamp = System.currentTimeMillis();
			(new TinySEExternalSort()).sort(initPath, trialPath, "./tmp", blocksize, nblocks);
			System.out.println("time duration: " + (System.currentTimeMillis() - timestamp) + " msecs with " + nblocks + " blocks of size " + blocksize + " bytes");
			trialUnit = toList(trialPath);
			if(trialView)
				printList(trialUnit,"Trial Test Units");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// answer
		Collections.sort(myUnit);
		b.createFile(answerPath, myUnit);
		if(answerView)
			printList(myUnit,"Answer Test Units");
		MutableTriple<Integer, Integer, Integer> before = trialUnit.get(0);
		for(int i=0; i<trialUnit.size(); i++){
			assertEquals(myUnit.get(i),trialUnit.get(i));
			assertTrue(trialUnit.get(i).compareTo(before) >= 0);
			before = trialUnit.get(i);
		}
	}
	
	public static void printList(Iterable<? extends MutableTriple<?,?,?>> l, String front){
		System.out.println(front + " START:");
		for(MutableTriple<?,?,?> t: l){
			System.out.print("(" + t.left + "," + t.middle + "," + t.right + ")");
		}
		System.out.println("\nEND.");
	}
	
	public static void printList(String path, String front){
		printList(toList(path),front);
	}
	
	public static ArrayList<MutableTriple<Integer,Integer,Integer>> toList(String path){
		ArrayList<MutableTriple<Integer,Integer,Integer>> trialUnit =
				new ArrayList<MutableTriple<Integer,Integer,Integer>>(unitcounts);
		for(int i=0; i<unitcounts; i++)
			trialUnit.add(new MutableTriple<Integer,Integer,Integer>(0,0,0));
		try {
			DiskIO.read_array(DiskIO.open_input_run(trialPath, blocksize), unitcounts, trialUnit);
		} catch (IOException e) {

		}
		return trialUnit;
	}

}
