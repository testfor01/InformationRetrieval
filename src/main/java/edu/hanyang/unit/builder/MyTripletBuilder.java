package edu.hanyang.unit.builder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.hanyang.utils.DiskIO;

public class MyTripletBuilder implements Builder<MyTriplet>{
	
	private int size;
	
	public static int limit = 10;
	
	public MyTripletBuilder(int size){
		this.size = size;
	}
	
	@Override
	public File createFile(String path, List<MyTriplet> in) {
		try {
			
			File file = new File(path);
			DataOutputStream fs = new DataOutputStream(new FileOutputStream(file));
			for(MyTriplet t : in){
				fs.writeInt(t.left);
				fs.writeInt(t.middle);
				fs.writeInt(t.right);
			}
			fs.close();
			return file;
		} catch (IOException e) {
			return null;
		}	
	}

	@Override
	public List<MyTriplet> createUnitTester() {
		List<MyTriplet> tr = new ArrayList<MyTriplet>();
		for(int i=0; i<size; i++){
			tr.add(new MyTriplet(generateInt(),generateInt(),generateInt()));
		}
		return tr;
	}
	
	private static int generateInt(){
		return (int)(Math.random()*limit);
	}

}
