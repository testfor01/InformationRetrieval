package edu.hanyang.unit.builder;


import org.apache.commons.lang3.tuple.MutableTriple;

public class MyTriplet extends MutableTriple<Integer, Integer, Integer>{

	private static final long serialVersionUID = 2801436097965862459L;

	public MyTriplet(int left, int middle, int right) {
		super(left,middle,right);
	}
	
	
}